Certgen is a tool for generating self signed client and server certificates.

# Installation

Once you have installed Go, run these commands to install the certgen package.
```shell
go get gitlab.com/sorenmat/certgen
```

# Running certgen

`certgen -hosts 127.0.0.1,example.com`

```shell
NAME:
   certgen - Utility for generating client / server certificates

USAGE:
   certgen [global options] command [command options] [arguments...]

VERSION:
   master

AUTHOR:
   Soren Mathiasen (sorenm@mymessages.dk)

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --hosts value       Comma-separated hostnames and IPs to generate a certificate for
   --name value        Name to prefix certificate and keys with (default: "test")
   --company value     Company name on the certtificate (default: "Giant Pants Inc.")
   --output value      Folder to write the certificates and keys in
   --start-date value  Creation date formatted as Jan 1 15:04:05 2011
   --duration value    Duration that certificate is valid for (default: 8760h0m0s)
   --help, -h          show help
   --version, -v       print the version
 ```


This project adheres to the Contributor Covenant [code of conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code.
We appreciate your contribution. Please refer to our [contributing guidelines](CONTRIBUTING.md).

[![Release](https://img.shields.io/github/release/caarlos0/example.svg?style=flat-square)](https://gitlab.com/sorenmat/certgen/releases/latest)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Travis](https://img.shields.io/travis/caarlos0/example.svg?style=flat-square)](https://travis-ci.org/sorenmat/certgen)
[![Coverage Status](https://img.shields.io/coveralls/caarlos0/examplesorenmat/ceretgen/master.svg?style=flat-square)](https://coveralls.io/gitlab/sorenmat/certgen?branch=master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/sorenmat/certgen?style=flat-square)](https://goreportcard.com/report/github.com/caarlos0/example)
[![Godoc](https://godoc.org/gitlab.com/sorenmat/certgen?status.svg&style=flat-square)](http://godoc.org/gitlab.com/sorenmat/certgen)
[![SayThanks.io](https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg?style=flat-square)](https://saythanks.io/to/sorenmat)
[![Powered By: GoReleaser](https://img.shields.io/badge/powered%20by-goreleaser-green.svg?style=flat-square)](https://github.com/goreleaser)