package certgen

import (
	"testing"
	"time"
)

func TestDatesHour(t *testing.T) {
	from, to := dates("Jan 01 12:00:00 2017", 4*time.Hour)
	if from.Hour() != 12 {
		t.Error("From should have been 12, was ", from.Hour())
	}
	if to.Hour() != 16 {
		t.Error("From should have been 16, was ", to.Hour())
	}

}

func TestDatesYear(t *testing.T) {
	from, to := dates("Jan 01 12:00:00 2017", 365*24*time.Hour)
	if from.Hour() != 12 {
		t.Error("From should have been 12, was ", from.Hour())
	}
	if to.Year() != 2018 {
		t.Error("From should have been 2018, was ", to.Year())
	}

}
