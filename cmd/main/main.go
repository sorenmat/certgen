package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/sorenmat/certgen"

	"github.com/caarlos0/spin"
	"github.com/urfave/cli"
)

var version = "master"

func main() {

	app := cli.NewApp()
	app.Name = "certgen"
	app.Version = version
	app.Author = "Soren Mathiasen (sorenm@mymessages.dk)"
	app.Usage = "Utility for generating client / server certificates"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "hosts",
			Value: "",
			Usage: "Comma-separated hostnames and IPs to generate a certificate for",
		},
		cli.StringFlag{
			Name:  "name",
			Value: "test",
			Usage: "Name to prefix certificate and keys with",
		},
		cli.StringFlag{
			Name:  "company",
			Value: "Giant Pants Inc.",
			Usage: "Company name on the certtificate",
		},
		cli.StringFlag{
			Name:  "output",
			Value: "",
			Usage: "Folder to write the certificates and keys in",
		},
		cli.StringFlag{
			Name:  "start-date",
			Value: "",
			Usage: "Creation date formatted as Jan 1 15:04:05 2011",
		},
		cli.DurationFlag{
			Name:  "duration",
			Value: 365 * 24 * time.Hour,
			Usage: "Duration that certificate is valid for",
		},
	}
	app.Action = func(c *cli.Context) error {
		if len(c.String("hosts")) == 0 {
			log.Fatalln("hosts is required")

		}

		spin := spin.New("\033[36m %s Working...\033[m")
		spin.Start()
		certgen.GenerateCerts(c.String("validFrom"), c.Duration("duration"), c.String("name"), c.String("company"), c.String("hosts"), c.String("output"))
		spin.Stop()
		/*
			if err != nil {
				return err
			}
		*/
		fmt.Println("Done!")
		return nil
	}
	_ = app.Run(os.Args)
}
