// Generate a self-signed X.509 certificate for a TLS server. Outputs to
// 'cert.pem' and 'key.pem' and will overwrite existing files.

package certgen

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"log"
	"math/big"
	"net"
	"os"
	"strings"
	"time"
)

func dates(from string, validFor time.Duration) (time.Time, time.Time) {
	var notBefore time.Time
	if from == "" {
		notBefore = time.Now()
	} else {
		var err error
		notBefore, err = time.Parse("Jan 2 15:04:05 2006", from)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to parse creation date: %s\n", err)
			os.Exit(1)
		}
	}

	notAfter := notBefore.Add(validFor)
	return notBefore, notAfter
}

func GenerateCerts(validFrom string, validFor time.Duration, name string, company string, hosts string, output string) {
	fmt.Printf("Hosts: %v\n", hosts)
	hosts1 := strings.Split(hosts, ",")
	fmt.Printf("Hosts: %v\n", hosts1)

	priv, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		log.Fatalf("failed to generate private key: %s", err)
	}

	notBefore, notAfter := dates(validFrom, validFor)
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("failed to generate serial number: %s", err)
	}

	parent_template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{company},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageCertSign | x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		IsCA: true,
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{company + " client cert"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:    x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
	}

	for _, h := range hosts1 {
		if strings.Contains(h, "/") {
			// We assume this is a CIDR address
			cidrHosts, err := Hosts(h)
			if err != nil {
				log.Fatal("Assumed host was an CIDR, but couldn't parse it...")
			}
			for _, ch := range cidrHosts {
				if ip := net.ParseIP(ch); ip != nil {
					parent_template.IPAddresses = append(parent_template.IPAddresses, ip)
				} else {
					parent_template.DNSNames = append(parent_template.DNSNames, h)
				}
			}
		} else {
			if ip := net.ParseIP(h); ip != nil {
				parent_template.IPAddresses = append(parent_template.IPAddresses, ip)
			} else {
				parent_template.DNSNames = append(parent_template.DNSNames, h)
			}
		}
	}
	log.Println("Generating server certs")
	ca := writeCerts(name, output, parent_template, parent_template, priv, true)

	log.Println("Generating client certs")
	cert := writeCerts(name, output, template, parent_template, priv, false)

	// check signature
	ca_c, _ := x509.ParseCertificate(ca)
	cert2_c, _ := x509.ParseCertificate(cert)

	err3 := cert2_c.CheckSignatureFrom(ca_c)
	if err3 != nil {
		log.Fatal("certificates doesn't appear to be valid")
	}
	log.Println("Successfully generated certificates")
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func writeCerts(name, output string, template, parent_template x509.Certificate, priv *ecdsa.PrivateKey, server bool) []byte {
	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &parent_template, publicKey(priv), priv)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s %s %v", name, err, server)
	}

	certName := name + "-client.crt"
	if server {
		certName = name + "-server.crt"
	}
	if output != "" {
		certName = output + "/" + certName
	}

	certOut, err := os.Create(certName)
	if err != nil {
		log.Fatalf("failed to open cert.pem for writing: %s", err)
	}

	err = pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	if err != nil {
		log.Fatal("unable to encode certificate")
	}
	err = certOut.Close()
	if err != nil {
		log.Fatal("unable to close certificate")
	}

	log.Printf("written %v\n", certName)

	keyName := name + "-client.key"
	if server {
		keyName = name + "-server.key"
	}
	if output != "" {
		keyName = output + "/" + keyName
	}
	keyOut, err := os.OpenFile(keyName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Printf("failed to open %v for writing: %v", keyName, err)
		return nil
	}

	b, err := x509.MarshalECPrivateKey(priv)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to marshal private key: %v", err)
		os.Exit(2)
	}
	x := &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}

	err = pem.Encode(keyOut, x)
	if err != nil {
		log.Fatal("unable to encode private certificate")
	}

	err = keyOut.Close()
	if err != nil {
		log.Fatal("unable to close private certificate")
	}

	log.Printf("written %v\n", keyName)
	return derBytes
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}
func Hosts(cidr string) ([]string, error) {
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, err
	}

	var ips []string
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		ips = append(ips, ip.String())
	}
	// remove network address and broadcast address
	return ips[1 : len(ips)-1], nil
}
